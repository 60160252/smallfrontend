import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/products',
    name: 'products',
    component: () => import('../views/Products/index.vue')
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/Customers',
    name: 'customers',
    component: () => import('../views/Customers/index.vue')
  },
  {
    path: '/Users',
    name: 'users',
    component: () => import('../views/Users/index.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
